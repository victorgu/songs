import React from 'react'

const Header = () => {
  return (
    <div className="ui row">
      <h1>A list of songs with details of each one. Made with React and Redux : )</h1>
    </div>
  )
}

export default Header